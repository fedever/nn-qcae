# -*- coding: utf-8 -*-
import tensorflow as tf
from scipy import misc
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from PIL import Image
import os
from skimage import io
from numpy.random import RandomState
from scipy.stats import chi


# Training parameters
img_shape = (400,400,4)
kernel_size = 3
in_channels = 4 
out_channels = 32 
out_channels_2 = 64
out_channels_3 = 128
weight_size = (in_channels // 4, out_channels // 4, kernel_size, kernel_size)
generation_rate = 500 # One test picture will be generated every 'generation_rate'
num_epochs = 5000

def rgb2gray(rgb):
    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return np.repeat(gray[:, :, np.newaxis], 3, axis=2)

def QCAE_preprocess_img(img):
    # Add a 0 as real component if quaternions (padding)
    npad  = ((0, 0), (0, 0), (1, 0))
    img = np.pad(img, pad_width=npad, mode='constant', constant_values=0)
    # Channel first
    #train = np.transpose(train, (2,0,1))
    return img

def QCAE_back_preprocess_img(q_img):
   # Construct the quaternion image
    res_img = np.zeros((400,400,3))
    res_img[:,:,0] = q_img[:,:,1]
    res_img[:,:,1] = q_img[:,:,2]
    res_img[:,:,2] = q_img[:,:,3]
      
    return res_img

def get_dataset(path):
  train_set = []
  test_set = []
  for image in os.listdir(path):
    if image.endswith("png"):
      sample_image = Image.open(r''+path+image)
      sample_image = sample_image.resize((400,400))
      sample_image = np.asarray(sample_image)/255

      test_image = QCAE_preprocess_img(sample_image)
      train_image = QCAE_preprocess_img(rgb2gray(sample_image))
      test_set.append(test_image)
      train_set.append(train_image)
      
  return np.asarray(train_set).astype('float32'), np.asarray(test_set).astype('float32')

def plot(losses):
    plt.plot(losses)
    plt.show()

def hard_tanh(x, name='htanh'):
    return tf.clip_by_value(x, -1, 1, name=name)

def initialize_weight(in_channels, out_channels, kernel_size):
    receptive_field = np.prod(kernel_size)
    fan_in = in_channels * receptive_field
    fan_out = out_channels * receptive_field
    s1 =  1./np.sqrt(2*(fan_in+fan_out))
    s2 =  1./np.sqrt(2*fan_in)
    if s2 > s1 :
        max_s, min_s = s2, s1
    else:
        max_s, min_s = s1, s2
    s = np.random.uniform(low=min_s, high=max_s, size=(1,))[0]
    
    rng = RandomState(np.random.randint(0,1234))
    kernel_shape =  (kernel_size, kernel_size) + (*(in_channels,out_channels),)
    
    modulus = chi.rvs(4,loc=0,scale=s,size=kernel_shape)
    number_of_weights = np.prod(kernel_shape) 
    v_i = np.random.normal(0,1.0,number_of_weights)
    v_j = np.random.normal(0,1.0,number_of_weights)
    v_k = np.random.normal(0,1.0,number_of_weights)
    
    # Purely imaginary quaternions unitary
    for i in range(0, number_of_weights):
        norm = np.sqrt(v_i[i]**2 + v_j[i]**2 + v_k[i]**2 +0.0001)
        v_i[i]/= norm
        v_j[i]/= norm
        v_k[i]/= norm
    v_i = v_i.reshape(kernel_shape)
    v_j = v_j.reshape(kernel_shape)
    v_k = v_k.reshape(kernel_shape)

    phase = rng.uniform(low=-np.pi, high=np.pi, size=kernel_shape)

    weight_r = modulus * np.cos(phase)
    weight_i = modulus * v_i*np.sin(phase)
    weight_j = modulus * v_j*np.sin(phase)
    weight_k = modulus * v_k*np.sin(phase)
    
    return weight_r.astype('float32'), weight_i.astype('float32'), weight_j.astype('float32'), weight_k.astype('float32')

def conv2d(kernel_size, in_channels, out_channels, x, name):
    # Instantiating the weights 
    r_weight, i_weight, j_weight, k_weight = initialize_weight(in_channels // 4, out_channels // 4, kernel_size)
    #initialize the weights
    '''weight_size = [out_channels//4,in_channels//4,kernel_size,kernel_size]
    glorat_normal =  1./np.sqrt(2*(in_channels//4+out_channels//4))
    r_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    i_weight  = tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    j_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    k_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)'''
    
    # Instantiating the channels transposed
    cat_kernels_4_r = tf.concat([r_weight, -i_weight, -j_weight, -k_weight], axis=1)
    cat_kernels_4_i = tf.concat([i_weight,  r_weight, -k_weight, j_weight], axis=1)
    cat_kernels_4_j = tf.concat([j_weight,  k_weight, r_weight, -i_weight], axis=1)
    cat_kernels_4_k = tf.concat([k_weight,  -j_weight, i_weight, r_weight], axis=1)
    cat_kernels_4_quaternion = tf.concat([cat_kernels_4_r, cat_kernels_4_i, cat_kernels_4_j, cat_kernels_4_k], axis=0)

    # reshaping and transforming in a variable
    cat_kernels_4_quaternion = tf.reshape(cat_kernels_4_quaternion, [kernel_size, kernel_size, in_channels, out_channels])
    weights = tf.get_variable('w'+name, initializer=cat_kernels_4_quaternion)
    
    bias = tf.get_variable('b'+name, shape=(weights.shape[3].value), initializer=tf.zeros_initializer())

    #bias  =  tf.random_uniform([weights.shape[3].value])

    # Layer
    x = tf.nn.conv2d(x, weights, strides=[1,1,1,1], padding='SAME')
    #x = tf.layers.batch_normalization(x)
    x = tf.contrib.layers.instance_norm(x)
    x = tf.nn.bias_add(x,bias)
    
    return x

def conv2dTranspose(kernel_size, in_channels, out_channels, x, output_shape, name):
    # Instantiating the weights
    r_weight, i_weight, j_weight, k_weight = initialize_weight(in_channels // 4, out_channels // 4, kernel_size)
    #initialize the weights
    '''weight_size = [out_channels//4,in_channels//4,kernel_size,kernel_size]
    glorat_normal =  1./np.sqrt(2*(in_channels//4+out_channels//4))
    r_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    i_weight  = tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    j_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)
    k_weight  =  tf.random_normal(weight_size,stddev=glorat_normal, dtype=tf.float32)'''
    
    # Instantiating the channels transposed
    cat_kernels_4_r = tf.concat([r_weight, -i_weight, -j_weight, -k_weight], axis=1)
    cat_kernels_4_i = tf.concat([i_weight,  r_weight, -k_weight, j_weight], axis=1)
    cat_kernels_4_j = tf.concat([j_weight,  k_weight, r_weight, -i_weight], axis=1)
    cat_kernels_4_k = tf.concat([k_weight,  -j_weight, i_weight, r_weight], axis=1)
    cat_kernels_4_quaternion = tf.concat([cat_kernels_4_r, cat_kernels_4_i, cat_kernels_4_j, cat_kernels_4_k], axis=0)
    
    # reshaping and transforming in a variable
    cat_kernels_4_quaternion = tf.reshape(cat_kernels_4_quaternion, [kernel_size, kernel_size, out_channels, in_channels])
    weights = tf.get_variable('w'+name, initializer=cat_kernels_4_quaternion)

    bias = tf.get_variable('b'+name, shape=(weights.shape[2].value), initializer=tf.zeros_initializer())
    #bias  =  tf.random_normal([weights.shape[2].value])
    
    # Layer
    x = tf.nn.conv2d_transpose(x, weights, strides=[1,1,1,1], padding='SAME', output_shape=output_shape)
    #x = tf.layers.batch_normalization(x)
    x = tf.contrib.layers.instance_norm(x)
    x = tf.nn.bias_add(x,bias)
    
    return x

def model(X):
    with tf.variable_scope('model'):
      # Encoding
      conv2d_layer_1 = tf.nn.tanh(conv2d(kernel_size, in_channels, out_channels, X, '1'))
      conv2d_layer_2 = tf.nn.tanh(conv2d(kernel_size, out_channels, out_channels_2, conv2d_layer_1, '2'))
      
      # Decoding
      conv2d_layer_transpose_1 = tf.nn.tanh(conv2dTranspose(kernel_size, out_channels_2, out_channels, conv2d_layer_2, tf.shape(conv2d_layer_1), '1_t'))
      conv2d_layer_transpose_2 = tf.nn.tanh(conv2dTranspose(kernel_size, out_channels, in_channels, conv2d_layer_transpose_1, X.shape, '2_t'))
      
      #return conv2d_layer_transpose_2
      return tf.nn.sigmoid(conv2d_layer_transpose_2)


if __name__ == '__main__':
	path = '/content/drive/My Drive/KODAK/'
	train, test = get_dataset(path)
	X = tf.placeholder(tf.float32, shape=[1, img_shape[0], img_shape[1], img_shape[2]])  # [#batch, img_height, img_width, #channels]
	Y = tf.placeholder(tf.float32, shape=[1, img_shape[0], img_shape[1], img_shape[2]])  # [#batch, img_height, img_width, #channels]

	pred = model(X)

	#MSE
	loss  = tf.losses.mean_squared_error(pred, Y)
	optimizer = tf.train.AdamOptimizer(learning_rate=0.0001).minimize(loss)

	# Run Session
	loss_to_epoch = []
	psnr = []
	ssim = []
	with tf.Session() as sess:
    	saver = tf.train.Saver() 
    	# Initialize Variables
    	sess.run(tf.global_variables_initializer())
		# Training
    	for epoch in tqdm(range(num_epochs)):
        	# Using only one image as training
        	train_input = np.expand_dims(train[0], axis=0)
        	loss_value, opt = sess.run([loss,optimizer], feed_dict={X:train_input,Y:train_input})
        	loss_to_epoch.append(np.average(loss_value))
        

       		 # If generation rate, generate a test image
        	if (epoch%generation_rate) == 0:
            	test_input = np.expand_dims(test[-1], axis=0)
            	test_output = sess.run(pred,feed_dict={X:test_input})
            	#Saving images output
            	q_img = QCAE_back_preprocess_img(test_output[-1])
            	plt.imsave("save_image"+str(epoch)+".png", q_img)
            	#Saving measures 
            	psnr.append(tf.image.psnr(test_output[-1], np.squeeze(test_input), max_val = 1.0).eval())
            	ssim.append(tf.image.ssim(tf.convert_to_tensor(test_output[-1]), tf.convert_to_tensor(np.squeeze(test_input)), max_val = 1.0).eval())
            	#Save Model
            	saver.save(sess, 'model_QCAE.ckpt')
	
	#See how our model performs
	sess = tf.InteractiveSession()
	saver = tf.train.Saver()
	saver.restore(sess, "model_QCAE.ckpt")

	test_input = np.expand_dims(test[1], axis=0)
	test_output = sess.run(pred,feed_dict={X:test_input})
	q_img = QCAE_back_preprocess_img(test_output[0])
	plt.imshow(q_img)
	
	# Plot metrics
	plot(loss_to_epoch)
	plot(ssim)
	plot(psnr)